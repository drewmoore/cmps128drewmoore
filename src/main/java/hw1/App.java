package hw1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}


	/**
	 * responds to GET on /hello
	 * Since no implementation is given for POST, 405 will be returned automatically
	 * @return "Hello world!"
     */
	@RequestMapping(value = "hello", method = RequestMethod.GET)
	public String hello(){
		return "Hello world!";
	}

	/**
	 * responds to GET on /echo
	 * @param msg (optional) the message to echo back
	 * @return the message received
     */
    @RequestMapping(value = "echo", method = RequestMethod.GET)
    public String echo(@RequestParam(required = false, value = "msg", defaultValue = "") String msg){
        return msg;
    }


}

