This is a standard maven (overly verbose Java) package structure, with application source in src/main/java.

**please cd into /target/docker before executing docker commands**: I've included a prebuilt .jar file there along with dockerfile.

If you'd like to repackage the sources (and you have maven installed) mvn package docker:build will repackage and replace the contents of /target/docker

Just in case... the commands I've tested are:

docker build .
docker run anamoore/hw1